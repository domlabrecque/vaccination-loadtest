Reference: https://github.com/heoelri/locust-on-aci 

# Locust on Azure Container Instances (ACI)

Load testing with [Locust](https://locust.io) using Azure Container Instances (ACI). This repository contains sample implementations in Terraform how to deploy and run load tests with Locust.

## Locust deployment via Terraform

This repository contains two different Terraform definitions to deploy Locust:

* headless - To conduct fully automated load tests without a user interface.
* webui - To deploy a load testing infrastructure with multiple worker nodes and a webui to conduct and monitor tests.

## Locust deployment pipelines

### Headless

The "headless" workflow asks upfront for all required information to conduct the load test:

* Number of Locust worker nodes
* Duration in minutes
* The rate per second in which users are spawned
* Number of concurrent Locust users
* Locust target URL

The workflow will then, based on your selection, deploy the required infrastructure in Azure, conduct the load test as defined, store the results in it's Azure Storage Account and scale down the infrastructure back to 0 - except its Storage Account.

### WebUI

The "webui" workflow spins up a full Locust deployment with the selected number of worker nodes. You can run the same pipeline again to scale the infrastructure down.

#### Steps to build
Make sure you're using the right subscription.
```
az login
```

In the same folder as main.tf (src/webui):
```
// Format all .tf files
terraform fmt -list -diff

// Initialize directory containing .tf config files
terraform init

// Validate
terraform validate

// Plan
terraform plan -input=false -out=tf_plan -var "prefix=$PREFIX" -var="locustWorkerNodes=$locustWorkerNodes"

// Apply
terraform apply tf_plan
```
To get the URL and password:
```
terraform output locust_webui_fqdn
terraform output locust_webui_credentials 
```

The locust service is mapped to port 8089.
Enter the url with the port in a browser to access the master worker:  
[PREFIX]-locust.canadaeast.azurecontainer.io:8089  
Enter username and password:

* Username: locust  
* Password: value of locust_webui_credentials

---

![locust webui architecture overview](img/locust-webui-architecture.png)
