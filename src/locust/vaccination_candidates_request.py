
import logging
import random
import os
import json
import pathlib
from locust import HttpUser, TaskSet, task, between


class ValidateCandidate(TaskSet):
    api_endpoint = "/api/v1.3/candidates/request"
    
    def on_start(self):
        
        file_path = pathlib.Path(__file__).parent.resolve()
        valid_data_path = os.path.join(file_path, 'valid_scenarios.json')
        invalid_data_path = os.path.join(file_path, 'invalid_scenarios.json')
        
        self.valid_data = json.load(open(valid_data_path, 'r'))
        self.invalid_data = json.load(open(invalid_data_path, 'r'))

    @task(2)
    def post_valid_candidates(self):
        candidate = random.choice(self.valid_data)
        r = self.client.post(self.api_endpoint, json=candidate, name=f'{self.api_endpoint} - valid candidate')
        # json_r = r.json()
        logging.info('HIN: %s - status: %s - ', candidate['healthInsuranceNumber'], r.status_code)

    @task(1)
    def post_invalid_form(self):
        candidate = random.choice(self.invalid_data)
        r = self.client.post(self.api_endpoint, json=candidate, name=f'{self.api_endpoint} - invalid candidate')
        logging.info('HIN: %s - status: %s - ', 'test_invalid', r.status_code)


class VaccinationUser(HttpUser):
    tasks = [ValidateCandidate]
    wait_time = between(1,5)