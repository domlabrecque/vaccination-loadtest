resource "azurerm_storage_account" "deployment" {
  name                     = "storage${var.prefix}"
  location                 = azurerm_resource_group.deployment.location
  resource_group_name      = azurerm_resource_group.deployment.name
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = local.default_tags
}

resource "azurerm_storage_share" "locust" {
  name                 = "locust"
  storage_account_name = azurerm_storage_account.deployment.name
  quota                = 50
}

resource "azurerm_storage_share_file" "locust_file" {
  name             = "vaccination_candidates_request.py"
  storage_share_id = azurerm_storage_share.locust.id
  source           = "../../locust/vaccination_candidates_request.py"
}

resource "azurerm_storage_share_file" "valid_scenarios" {
  name             = "valid_scenarios.json"
  storage_share_id = azurerm_storage_share.locust.id
  source           = "../../locust/valid_scenarios.json"
}

resource "azurerm_storage_share_file" "invalid_scenarios" {
  name             = "invalid_scenarios.json"
  storage_share_id = azurerm_storage_share.locust.id
  source           = "../../locust/invalid_scenarios.json"
}