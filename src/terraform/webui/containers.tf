resource "azurerm_container_group" "master" {
  count               = var.locustWorkerNodes >= 1 ? 1 : 0
  name                = "${var.prefix}-locust-master"
  location            = azurerm_resource_group.deployment.location
  resource_group_name = azurerm_resource_group.deployment.name
  ip_address_type     = "Public"
  dns_name_label      = "${var.prefix}-locust"
  os_type             = "Linux"

  container {
    name   = "${var.prefix}-locust-master"
    image  = var.locustVersion
    cpu    = "2"
    memory = "2"


    commands = [
      "locust",
      "--locustfile",
      "/home/locust/locust/${azurerm_storage_share_file.locust_file.name}",
      "--master",
      "--web-auth",
      "locust:${azurerm_key_vault_secret.locustsecret.value}",
      "--host",
      var.targeturl
    ]

    volume {
      name       = "locust"
      mount_path = "/home/locust/locust"

      storage_account_key  = azurerm_storage_account.deployment.primary_access_key
      storage_account_name = azurerm_storage_account.deployment.name
      share_name           = azurerm_storage_share.locust.name
    }

    ports {
      port     = "8089"
      protocol = "TCP"
    }

    ports {
      port     = "5557"
      protocol = "TCP"
    }

  }

  tags = local.default_tags
}

resource "azurerm_container_group" "worker" {
  count               = var.locustWorkerNodes
  name                = "${var.prefix}-locust-worker-${count.index}"
  location            = var.locustWorkerLocations[count.index % length(var.locustWorkerLocations)]
  resource_group_name = azurerm_resource_group.deployment.name
  ip_address_type     = "Public"
  os_type             = "Linux"

  container {
    name   = "${var.prefix}-locust-worker-${count.index}"
    image  = var.locustVersion
    cpu    = "2"
    memory = "2"

    commands = [
      "locust",
      "--locustfile",
      "/home/locust/locust/${azurerm_storage_share_file.locust_file.name}",
      "--worker",
      "--master-host",
      azurerm_container_group.master[0].fqdn
    ]

    volume {
      name       = "locust"
      mount_path = "/home/locust/locust"

      storage_account_key  = azurerm_storage_account.deployment.primary_access_key
      storage_account_name = azurerm_storage_account.deployment.name
      share_name           = azurerm_storage_share.locust.name
    }

    ports {
      port     = 8089
      protocol = "TCP"
    }

  }

  tags = local.default_tags
}