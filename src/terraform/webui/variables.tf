variable "location" {
  description = "The Azure Region in which the master and the shared storage account will be provisioned."
  type        = string
  default     = "canadaeast"
}

variable "environment" {
  description = "Environment Resource Tag"
  type        = string
  default     = "dev"
}

variable "locustVersion" {
  description = "Locust Container Image Version"
  type        = string
  default     = "locustio/locust:2.2.1"
}

variable "targeturl" {
  description = "Target URL"
  type        = string
  default     = "https://aki-vacc-ca-stg.akinox.dev"
}

variable "locustWorkerNodes" {
  description = "Number of Locust worker instances (zero will stop master)"
  type        = string
  default     = "0"
}

variable "locustWorkerLocations" {
  description = "List of regions to deploy workers to in round robin fashion"
  type        = list(any)
  default     = ["canadacentral", "canadaeast"]
}

variable "prefix" {
  description = "A prefix used for all resources in this example. Must not contain any special characters. Must not be longer than 10 characters."
  type        = string
  validation {
    condition     = length(var.prefix) >= 5 && length(var.prefix) <= 12
    error_message = "Prefix must be between 5 and 10 characters long."
  }
}
