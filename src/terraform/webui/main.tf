terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.71.0"
    }
  }
}

provider "azurerm" {
  skip_provider_registration = true
  features {}
}

data "azurerm_client_config" "current" {
}

resource "azurerm_resource_group" "deployment" {
  name     = "locust-${var.prefix}-rg"
  location = var.location
  tags     = local.default_tags
}