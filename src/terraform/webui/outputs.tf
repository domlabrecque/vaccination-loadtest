output "locust_webui_fqdn" {
  value = azurerm_container_group.master.*.fqdn
}

output "locust_webui_credentials" {
  value     = azurerm_key_vault_secret.locustsecret.value
  sensitive = true
}
